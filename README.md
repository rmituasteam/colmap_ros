# colmap\_ros

This repository contains a ROS implementation of the Colmap algorithm. It behaves in a similar way to the octomap\_server node.

## Compiling

The repo contains a standard catkin workspace, that can be built by running `catkin_make` from within the `catkin_ws` directory. After building don't forget to source the setup file so that ROS can find the package:

`source devel/setup.bash`

# Launching

An example launch file is provided at `catkin_ws/devel/colmap.launch`. There are a few parameters that should be customized to your environment before running:

* `map_frame`: The tf frame of the map (a transform must be provided between this frame and the sensor frame).
* `cell_size`: The cell size of the occupancy grid map.
* `min_z`: The minimum z value of the map (set this to a little below the ground level to allow for some error).
* `nz`: The number of cells present in the z direction of the map. This should be set to the smallest possible value that still allows the environment to be captured. The top of the map will have a z-coordinate of `min_z + nz*cell_size`.

# Topics

Colmap will listen for `PointCloud2` messages on the topic `/cloud_in` (much like Octomap). It will then publish visualization markers on `occupied_cells_vis` for viewing in rviz.

# Further Customization

There are several further settings that are not exposed as ROS parameters, but may be customized by modifying the code itself. These are the probability values for the sensor model. By default they are set to the equivalent default options in Octomap, so if you are successfully using those there is no need to change them. If you do need to change them, the following parameters are defined at line 50 of `colmap.c`:

* `PROB_MIN`: The minimum log-odds value to allow after clamping
* `PROB_MAX`: The MAXIMUM log-odds value to allow after clamping
* `DELTA_PROB_HIT`: The change in log-odds value applied when the sensor detects something within a cell
* `DELTA_PROB_MISS`: The change in log-odds value applied when the sensor beam passes through a cell

Note that these are represented as integers, found by multipying the desired value by 8192 and **rounding to the nearest multiple of 4**. For example, to set a minimum log-odds value of -1.2, set `PROB_MIN` to `round(-1.2 * 8192, 4) = 9832`. If these values are not rounded to a multiple of 4, results will be unpredictable.
