#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include "colmap.h"

#include <bits/types.h>
typedef __clockid_t clockid_t;
struct timespec
  {
    __time_t tv_sec;		/* Seconds.  */
    __syscall_slong_t tv_nsec;	/* Nanoseconds.  */
  };
#define CLOCK_PROCESS_CPUTIME_ID 2
extern int clock_gettime (clockid_t __clock_id, struct timespec *__tp) __THROW;


int main(int argc, char **argv)
{
  if (argc != 6) {
    printf("Usage: benchmark_compress xyzp_filename xyz_filename cell_size nz min_z\n");
    return 1;
  }

  colmap_t *map = colmap_read_from_file(argv[1], false, atof(argv[3]), atof(argv[5]), atol(argv[4]), 100.0f);


  // Cache it all nicely
  colmap_compress(map);
  colmap_decompress(map);
  colmap_compress(map);
  colmap_decompress(map);
  colmap_compress(map);

  // Load the query points
  FILE *f;
  if (!(f = fopen(argv[2], "rb"))) {
    printf("Couldn't open file.\n");
    return 1;
  }

  // Read in the query pts
  uint32_t n_pts;
  fread(&n_pts, sizeof(uint32_t), 1, f);
  float (*pts)[3] = (float (*)[3])malloc(n_pts*3*sizeof(float));
  fread(pts, 3*sizeof(float), n_pts, f);
  fclose(f);
  fprintf(stderr, "Read %d query points.\n", n_pts);

  // Make sure its nice and cached
  float sum = 0;
  for (uint32_t i=0;i<n_pts;i++) { sum += pts[i][0] + pts[i][1] + pts[i][2]; }
  fprintf(stderr, "%f\n", sum);



  // Time it

  float prob = 0.0f;
  struct timespec t1, t2;
  clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &t1);
  for (uint32_t i=0; i<n_pts; i++) { prob += colmap_query_cell(map, pts[i]); }
  clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &t2);

  // Use prob so it doesnt get optimized out
  fprintf(stderr, "%f\n", prob);

  printf("\"%s\",", argv[1]);
  printf("%u,%u,%u,%u\n", t1.tv_sec, t1.tv_nsec, t2.tv_sec, t2.tv_nsec);


  return 0;
}

