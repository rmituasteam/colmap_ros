
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include "colmap.h"

#include <bits/types.h>
typedef __clockid_t clockid_t;
struct timespec
  {
    __time_t tv_sec;		/* Seconds.  */
    __syscall_slong_t tv_nsec;	/* Nanoseconds.  */
  };
#define CLOCK_PROCESS_CPUTIME_ID 2
extern int clock_gettime (clockid_t __clock_id, struct timespec *__tp) __THROW;


int main(int argc, char **argv)
{
  if (argc != 5) {
    printf("Usage: benchmark_compress xyzp_filename cell_size nz min_z\n");
    return 1;
  }

  colmap_t *map = colmap_read_from_file(argv[1], false, atof(argv[2]), atof(argv[4]), atol(argv[3]), 100.0f);

  // Cache it all nicely
  colmap_compress(map);
  colmap_decompress(map);
  colmap_compress(map);
  colmap_decompress(map);
  colmap_compress(map);

  // Time it

  struct timespec t1, t2;

  clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &t1);
  for (int i=0; i<10; i++) {
    colmap_decompress(map);
    colmap_compress(map);
  }
  clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &t2);

  printf("\"%s\",", argv[1]);
  printf("%u,%u,%u,%u\n", t1.tv_sec, t1.tv_nsec, t2.tv_sec, t2.tv_nsec);


  return 0;
}

