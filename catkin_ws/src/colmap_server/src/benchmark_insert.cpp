#include <string.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>

#include <octomap/octomap.h>
#include <octomap/octomap_timing.h>

#include "colmap.h"

using namespace std;
using namespace octomap;


int main(int argc, char** argv)
{

  if (argc != 5) {
    printf("Usage: benchmark_insert graph_filename cell_size nz min_z\n");
    return 1;
  }

  string inputFilename = argv[1];
  cout << "\"" << inputFilename << "\",";

  ScanGraph graph;
  if (!graph.readBinary(inputFilename)) {
    cout << "Couldn't read" << endl;
    return 1;
  }

  // Convert scan format
  graph.transformScans();
  uint32_t n_scans = graph.size();

  float (*origin)[3] =(float (*)[3]) malloc(n_scans*8*sizeof(float));
  uint32_t *n_pts = (uint32_t*)malloc(n_scans*sizeof(uint32_t));
  float **pts = (float**)malloc(n_scans*sizeof(float*));

  uint32_t i = 0;
  for (ScanGraph::iterator scan_it = graph.begin(); scan_it != graph.end(); scan_it++) {

    origin[i][0] = (*scan_it)->pose.trans().x();
    origin[i][1] = (*scan_it)->pose.trans().y();
    origin[i][2] = (*scan_it)->pose.trans().z();

    n_pts[i] = (*scan_it)->scan->size();
    pts[i] = (float*)malloc(n_pts[i]*3*sizeof(float));

    for (int j=0; j<n_pts[i]; j++) {
      pts[i][j*3] = (*((*scan_it)->scan))[j].x();
      pts[i][j*3+1] = (*((*scan_it)->scan))[j].y();
      pts[i][j*3+2] = (*((*scan_it)->scan))[j].z();
    }
    i++;
  }

  // First with pruning

  colmap_t *map = colmap_create(atof(argv[2]), atof(argv[4]), atol(argv[3]), 100.0f);
  struct timespec t1, t2;
  clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &t1);
  for (int i=0; i<n_scans; i++) {
    colmap_insert_pcl(map, origin[i], (float (*)[3])(pts[i]), n_pts[i]);
    colmap_compress(map);
  }
  clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &t2);


  colmap_stats_t s;
  colmap_get_stats(map, &s);
  printf("\n\n%u,%u,%u,%u,%u,%u,%u,\n\n", map->nz, s.n_comp_leafs, s.n_uncomp_leafs, s.n_inner_nodes, s.comp_leaf_n_runs, s.comp_leaf_size32, s.comp_leaf_size16);

  colmap_delete(map);

  cout << t1.tv_sec << "," << t1.tv_nsec << "," << t2.tv_sec << "," << t2.tv_nsec << ",";


  // Then without pruning

  map = colmap_create(atof(argv[2]), atof(argv[4]), atol(argv[3]), 100.0f);
  clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &t1);
  for (int i=0; i<n_scans; i++) {
    colmap_insert_pcl(map, origin[i], (float (*)[3])(pts[i]), n_pts[i]);
  }
  clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &t2);

  colmap_get_stats(map, &s);
  printf("\n\n%u,%u,%u,%u,%u,%u,%u,\n\n", map->nz, s.n_comp_leafs, s.n_uncomp_leafs, s.n_inner_nodes, s.comp_leaf_n_runs, s.comp_leaf_size32, s.comp_leaf_size16);

  colmap_delete(map);

  cout << t1.tv_sec << "," << t1.tv_nsec << "," << t2.tv_sec << "," << t2.tv_nsec << endl;

}

