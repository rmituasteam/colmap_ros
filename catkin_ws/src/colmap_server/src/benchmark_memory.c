#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include "colmap.h"

int main(int argc, char **argv)
{
  if (argc != 4) {
    printf("Usage: mem_usage xyzp_filename cell_size nz\n");
    return 1;
  }

  printf("\"%s\",", argv[1]);

  colmap_stats_t s;

  colmap_t *map = colmap_read_from_file(argv[1], false, atof(argv[2]), 0.0f, atol(argv[3]), 10.0f);

  colmap_compress(map);
  colmap_get_stats(map, &s);
  printf("%u,%u,%u,%u,%u,%u,%u,", map->nz, s.n_comp_leafs, s.n_uncomp_leafs, s.n_inner_nodes, s.comp_leaf_n_runs, s.comp_leaf_size32, s.comp_leaf_size16);

  colmap_decompress(map);
  colmap_get_stats(map, &s);
  printf("%u,%u,%u,%u,%u,%u,%u,", map->nz, s.n_comp_leafs, s.n_uncomp_leafs, s.n_inner_nodes, s.comp_leaf_n_runs, s.comp_leaf_size32, s.comp_leaf_size16);

  colmap_delete(map);
  map = colmap_read_from_file(argv[1], true, atof(argv[2]), 0.0f, atol(argv[3]), 10.0f);

  colmap_compress(map);
  colmap_get_stats(map, &s);
  printf("%u,%u,%u,%u,%u,%u,%u,", map->nz, s.n_comp_leafs, s.n_uncomp_leafs, s.n_inner_nodes, s.comp_leaf_n_runs, s.comp_leaf_size32, s.comp_leaf_size16);

  colmap_decompress(map);
  colmap_get_stats(map, &s);
  printf("%u,%u,%u,%u,%u,%u,%u\n", map->nz, s.n_comp_leafs, s.n_uncomp_leafs, s.n_inner_nodes, s.comp_leaf_n_runs, s.comp_leaf_size32, s.comp_leaf_size16);

  return 0;
}

