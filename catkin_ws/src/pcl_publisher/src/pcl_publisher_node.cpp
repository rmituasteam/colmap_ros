#include <ros/ros.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>

typedef pcl::PointCloud<pcl::PointXYZ> PointCloud;

int main(int argc, char **argv)
{
  ros::init(argc, argv, "pcl_publisher");
  ros::NodeHandle nh;
  ros::Publisher pub = nh.advertise<PointCloud>("velodyne_points", 1);

  PointCloud::Ptr msg(new PointCloud);
  msg->header.frame_id = "optitrack_origin";

  for (double t = 0.0; t <= 2.0*M_PI; t+=0.1) {
    msg->points.push_back(pcl::PointXYZ(cos(t), sin(t), cos(t)*sin(t)));
  }

  msg->height = 1;
  msg->width = msg->points.size();

  ros::Rate loop_rate(0.1);
  while (nh.ok()) {
    pcl_conversions::toPCL(ros::Time::now(), msg->header.stamp);
    pub.publish(msg);
    ros::spinOnce();
    loop_rate.sleep();
  }
}

